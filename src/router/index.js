import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Eventos from '../views/Eventos.vue'
import Evento from '../views/Evento.vue'
import nuevoEvento from "../views/nuevoEvento.vue"
import actualizarEvento from "../views/actualizarEvento.vue"
import Publicaciones from "../views/Publicaciones.vue"
import Publicacion from "../views/Publicacion.vue"
import nuevaPublicacion from "../views/nuevaPublicacion.vue"
import actualizarPublicacion from "../views/actualizarPublicacion.vue"
import Discusiones from "../views/Discusiones.vue";
import Discusion from "../views/Discusion.vue"
import nuevaDiscusion from "../views/nuevaDiscusion.vue"

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: "/eventos",
    name: "Eventos",
    component: Eventos
  },
  {
    path: "/publicaciones",
    name: "Publicaciones",
    component: Publicaciones
  },
  {
    path: "/discusiones",
    name: "Discusiones",
    component: Discusiones
  },
  {
    path: "/eventos/:id",
    name: "Evento",
    component: Evento
  },
  {
    path: "/publicaciones/:id",
    name: "Publicacion",
    component: Publicacion
  },
  {
    path: "/discusiones/:id",
    name: "Discusion",
    component: Discusion
  },
  {
    path: "/eventos/c/nuevo", // 'c' de create
    name: "nuevoEvento",
    component: nuevoEvento
  },
  {
    path: "/publicaciones/c/nuevo",
    name: "nuevaPublicacion",
    component: nuevaPublicacion
  },
  {
    path: "/discusiones/c/nuevo",
    name: "nuevaDiscusion",
    component: nuevaDiscusion
  },
  {
    path: "/eventos/r/actualizar/:id", // 'r' de replace
    name: "actualizarEvento",
    component: actualizarEvento
  },
  {
    path: "/publicaciones/r/actualizar/:id",
    name: "actualizarPublicacion",
    component: actualizarPublicacion
  }
]

const router = new VueRouter({
  routes
})

export default router
